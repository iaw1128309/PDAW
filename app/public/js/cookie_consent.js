/**
 * Created by iaw1128309 on 5/17/17.
 */
window.addEventListener("load", function () {
    window.cookieconsent.initialise({
        "palette": {
            "popup": {
                "background": "#ffc410"
            },
            "button": {
                "background": "#1d3156"
            }
        },
        "theme": "classic",
        "position": "top",
        "static": true,
        "content": {
            "message": "Este sitio web utiliza cookies para garantizar que obtenga la mejor experiencia en nuestro sitio web.",
            "dismiss": "Entendido",
            "link": "Más información",
            "href": "/privacy_policy"
        }
    })
});