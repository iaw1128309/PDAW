/**
 * Created by iaw1128309 on 4/26/17.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema; // Schema object

// Schema instance
var consultaSchema = new Schema({
    Asunto: String,
    Consulta: String,
    Es_privado: Boolean,
    Respuesta: String,
    Falta_para_revisar: Boolean, // Se crea cuando un usuario pone flag a una consulta
    Fecha_revisada: Date, // Se crea cuando un abogado revisa una consulta
    // Id_usuario: Schema.Types.ObjectId,
    // Id_abogado: Schema.Types.ObjectId
    Usuario: {
        Email: String
    },
    Abogado: {
        Email: String,
        Nombre: String,
        Apellidos: String
    }
});

// Register Usuarios collection with a schema and export (FINDS IN LOWERCASE)
module.exports = mongoose.model('Consultas', consultaSchema);