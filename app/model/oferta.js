/**
 * Created by iaw1128309 on 4/26/17.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema; // Schema object

// Schema instance
var ofertaSchema = new Schema({
    Puesto: String,                             //puesto a cubrir
    Descripcion: String,
    Plazas: Number,                             //numero de vacantes
    Duracion: Number,
    Jornada: String,                            //completa o parcial
    Lugar: String,
    Sueldo: Number,
    Requisitos: String,                         //los requisitos exigidos para realizar la tarea ofertada
    Fecha_publicacion: Date,
    Inscritos: [
        {
            _id: Schema.Types.ObjectId,
            Nombre_usuario: String,
            Apellidos_usuario: String,
            Email_usuario: String,
            Favorable: Number,                  //al validar su situación este campo valdra 1 si es favorable, 0 en caso contrario
            Entrevista: {
                Fecha_Entrevista: Date,     //al recibir email de entrevista, fecha_entrevista sera la fecha prevista para la entrevisata
                Estado: Number,             //estado inicial valdra 0, si le contratan valdra 1
                Descartado: Number        //en el caso de que no le contratan pero le han entrevistado, este campo existira y valdra 1

            }
        }],
    Usuarios_contratados: [Schema.Types.ObjectId],
    Id_empleador: Schema.Types.ObjectId
});

// Register Usuarios collection with a schema and export (FINDS IN LOWERCASE)
module.exports = mongoose.model('Ofertas', ofertaSchema);