/**
 * Created by iaw1128309 on 4/26/17.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema; // Schema object

// Schema instance
var usuarioSchema = new Schema({
    Nombre: String,
    Apellidos: String,
    Tipo: String,
    Email: String,
    Contrasena: String,
    Nif_empresa: String,
    // Ofertas: [{type: Schema.Types.ObjectId}],
    Ofertas_inscritas: {type: [Schema.Types.ObjectId], default: void 0},
    Oferta_contratada: Schema.Types.ObjectId,
    Dni_nie: String,
    Numero_colegiado: Number,
    No_verificado: Number, //hay que confirmar por enlace en correo
    Favorable: Number,       //al apuntarse en ofertas si los abogados dan el ok, este campo sera 1, si es ko sera 0
    Ultimo_flag: Date   // La ultima vez que el usuario ha enviado una consulta para revisar
});

// Register Usuarios collection with a schema and export (FINDS IN LOWERCASE)
module.exports = mongoose.model('Usuarios', usuarioSchema);