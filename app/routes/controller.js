var express = require('express');
var Usuarios = require('../model/usuario');
var Ofertas = require('../model/oferta');
var Consulta = require('../model/consulta');
var router = express.Router();
var bCrypt = require('bcrypt-nodejs');
var fs = require('fs-extra');
var rimraf = require('rimraf');
var mkdirp = require('mkdirp');


var ObjectId = require('mongodb').ObjectID, // para comprobar objectids
    nodemailer = require('nodemailer'),
    // Correo electronico para enviar mensajes
    transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'emigration2wiaw@gmail.com',
            pass: 'emigration2017'
        }
    });
var path = require('path');

var isAuthenticated = function (req, res, next) {
    // if user is authenticated in the session, call the next() to call the next request handler
    // Passport adds this method to request object. A middleware is allowed to add properties to
    // request and response objects
    if (req.isAuthenticated()) {
        Usuarios.findById(req.session.user._id, function (err, user) {
            if (err)
                console.log(err)
            req.session.user = user;
        })
        return next();
    }

    // if the user is not authenticated then redirect him to the login page
    req.session.returnTo = req.url; // Poner url que requiere login para poder volver a ella despues de identificarse
    res.redirect('/login');
};

module.exports = function (passport) {

    /** PASSPORT **/
    /* GET login page. */
    router.get('/login', function (req, res) {

        // Redirect a home si hay una sesión activa
        if (req.isAuthenticated()) {
            return res.redirect('/');
        }

        // Si el usuario quiere verificar su usuario registrado
        if (req.query.code) {

            // Comprobar si el codigo es un objectid valido
            try {
                ObjectId(req.query.code);
            } catch (err) {
                return res.render('loginpage', {error: 'El codigo de verificación es invalido'});
            }

            // Buscar en la bbdd el usuario para verificar
            Usuarios.findOneAndUpdate(
                {_id: ObjectId(req.query.code), No_verificado: 1},
                {$unset: {No_verificado: ""}},
                function (err, doc) {
                    if (err) {
                        // throw err;
                        console.log('ERROR BUSCANDO USUARIO PARA VERIFICAR\n' + err);
                        return res.render('loginpage', {error: 'Se ha producido un error en nuestros servidores. Por favor, intentalo más tarde'});
                    }

                    // Volver a login con error si el usuario para verificar ya está verificado
                    if (!doc) {
                        return res.render('loginpage', {error: 'El codigo de verificación es invalido'});
                    }

                    // Usuario verificado
                    console.log('Usuario verificado: ' + doc);
                    return res.render('loginpage', {message: 'Usuario verificado'});
                }
            );

            // Si el usuario ha solicitado el reenvio del mensaje de verificación
        } else if (req.query.email && req.query.send_code) {

            // Comprobar si el codigo es un objectid valido
            try {
                ObjectId(req.query.send_code);
            } catch (err) {
                return res.render('loginpage', {error: 'El codigo de verificación es invalido'});
            }

            // Mensaje para enviar
            var htmlMessage = '<h2>Bienvenido a E-Migration</h2>. <a href="http://localhost:3000/login?code=' + req.query.send_code
            var website = req.protocol + '://' + req.get('host'),
                htmlMessage = '<h2>Bienvenido a E-Migration</h2>' +
                    '<a href="' + website + '/login?code=' + req.query.send_code
                    + '">Clica aqui para verificar tu cuenta</a>',
                message = {
                    from: 'E-Migration <support@emigration.com>',
                    to: req.query.email,
                    subject: 'Verifica tu usuario en E-Migration',
                    html: htmlMessage
                };

            // Enviar el mensaje
            transporter.sendMail(message, function (err) {
                if (err) {
                    console.log('ERROR SENDING MAIL ' + err);
                    // throw err;
                    return res.render('loginpage', {error: 'Se ha producido un error en nuestros servidores. Por favor, intentalo más tarde'});
                }

                console.log('Mail sent');

                // Volver a login notificando al usuario sobre el mensaje reenviado
                return res.render('loginpage', {message: "Mensaje reenviado"});
            });

        } else { // GET normal de login
            // Display the Login page with any flash message, if any
            return res.render('loginpage', {message: req.flash('message'), error: req.flash('error')});
        }
    });

    /* Handle Login POST */
    router.post('/login', passport.authenticate('login', {
        successReturnToOrRedirect: '/index', // esta opción perimitirá volver a la ultima url visitada, o index
        failureRedirect: '/login',
        failureFlash: true
    }));

    /* GET Registration Page */
    router.get('/signup', function (req, res) {
        if (req.isAuthenticated()) {
            return res.redirect('/');
        }
        return res.render('register', {message: req.flash('message')});
    });

    /* Handle Registration POST */
    router.post('/signup', passport.authenticate('signup', {
        session: false,
        successRedirect: '/login',
        failureRedirect: '/signup',
        failureFlash: true
    }));

    /* GET Home Page */
    router.get(['/', '/index'], function (req, res) {
        res.render('index', {message: req.flash('message')});
    });

    /* Handle Logout */
    router.get('/logout', function (req, res) {
        req.logout();
        req.session.destroy();
        res.redirect('/');

    });
    /** END PASSPORT **/

    /** RESET PASSWORD **/

    // GET de reset password
    router.get('/reset_password', function (req, res) {

        // Redirect a home si hay una sesión activa
        if (req.isAuthenticated())
            return res.redirect('/');
        res.render('reset_password');
    });

    // Esta petición viene del mensaje enviado despúes de solicitar el reset de contraseña
    router.get('/reset_password/:code', function (req, res) {

        // Redirect a home si hay una sesión activa
        if (req.isAuthenticated())
            return res.redirect('/');

        // Comprobar si el codigo es un objectid valido
        try {
            ObjectId(req.params.code);
        } catch (err) {
            return res.render('reset_password', {error: 'Codigo invalido'});
        }

        // Comprobar si existe un usuario con este id en la bbdd
        Usuarios.findById(req.params.code, function (err, user) {
            if (err) {
                console.log('ERROR BUSCANDO USUARIO\n' + err);
                return res.render('reset_password', {
                    user_id: req.params.code,
                    error: 'Se ha producido un error en nuestros servidores. Por favor, intentalo más tarde'
                });
            }

            // Informar al usuario que el codigo es invalido si el usuario no existe
            if (!user) {
                return res.render('reset_password', {error: 'Codigo invalido'});
            }

            // Ir otra vez a reset_password con el id de usuario
            res.render('reset_password', {user_id: user._id});
        });
    });

    // POST de reset_password con el id de usuario en el url
    router.post('/reset_password/:code', function (req, res) {
        // Comprobar si ha seguido el requisito en contrasena
        var pattern = /.{6,}/;
        if (!pattern.test(req.body.password)) {
            return res.render('reset_password', {
                user_id: req.params.code,
                error: 'Contraseña invalida. Tiene que tener como minimo 6 caracteres'
            });
        }

        // Actualizar la contraseña del usuario
        Usuarios.findByIdAndUpdate(
            req.params.code,
            {$set: {Contrasena: bCrypt.hashSync(req.body.password, bCrypt.genSaltSync(10), null)}},
            function (err, user) {
                if (err) {
                    console.log('ERROR RESET DE CONTRASEÑA\n' + err);
                    return res.render('reset_password', {
                        user_id: req.params.code,
                        error: 'Se ha producido un error en nuestros servidores. Por favor, intentalo más tarde'
                    });
                }

                console.log('Contraseña cambiada\n' + user);
                // Ir a login e informar al usuario que se ha cambiado la contraseña
                return res.render('loginpage', {message: 'Contraseña cambiada correctamente'});
            })
    });

    router.post('/reset_password_check_email', function (req, res) {
        Usuarios.findOne({Email: req.body.email}, function (err, user) {
            if (err) {
                console.log('ERROR BUSCANDO USUARIO\n' + err);
                // throw err;
                return res.render('reset_password', {error: 'Se ha producido un error en nuestros servidores. Por favor, intentalo más tarde'});
            }

            if (!user) {
                return res.render('reset_password', {error: 'No existe ningun usuario con este email'});
            }

            // Mensaje para enviar
            var website = req.protocol + '://' + req.get('host'),
                htmlMessage = '<h2>Recuperar acceso a tu usuario en E-Migration</h2>' +
                    '<a href="' + website + '/reset_password/' + user._id
                    + '">Clica aqui para establecer una contraseña nueva para tu usuario</a>',
                message = {
                    from: 'E-Migration <support@emigration.com>',
                    to: user.Email,
                    subject: 'Recuperar acceso a tu usuario en E-Migration',
                    html: htmlMessage
                };

            // Enviar el mensaje
            transporter.sendMail(message, function (err) {
                if (err) {
                    console.log('ERROR SENDING MAIL ' + err);
                    return res.render('reset_password', {error: 'Se ha producido un error en nuestros servidores. Por favor, intentalo más tarde'});
                }

                console.log('Mail sent');

                // Volver a login notificando al usuario sobre el mensaje reenviado
                return res.render('loginpage', {message: "Mensaje enviado. Sigue las instrucciones en el mensaje para recuperar tu contraseña"});
            });
        });
    });
    /** RESET PASSWORD END **/


    //obetener la pagina de ofertas
    router.get('/empleo', isAuthenticated, function (req, res) {
        return Ofertas.find({Plazas: {$gt: 0}}, function (err, datos) {
            if (err) {
                return res.render('index',
                    {
                        error: 'Se ha producido un error en nuestros servidores, vuelve a intentarlo mas tarde!!'
                    });
            }


            if (req.query.server_error) {
                return res.render('empleo',
                    {
                        datos: datos,
                        error: 'Se ha producido un error en nuestros servidores, vuelve a intentarlo mas tarde!!'

                    });
            }
            return res.render('empleo', {
                datos: datos
            });
        }).sort({$natural: -1})
    });

    router.post('/empleo', function (req, res) {
        var newOferta = new Ofertas(
            {
                Descripcion: req.body.descripcion,
                Duracion: req.body.duracion,
                Sueldo: req.body.sueldo,
                Lugar: req.body.lugar,
                Plazas: req.body.plazas,
                Fecha_publicacion: new Date(),
                Jornada: req.body.jornada,
                Puesto: req.body.puesto,
                Requisitos: req.body.requisitos,
                Id_empleador: req.session.user._id
            }
        );

        console.log(newOferta);

        newOferta.save(function (err) {
            if (err) throw err;
            res.redirect('empleo')
        });

    });

    router.post('/detalles_oferta', function (req, res) {
        Ofertas.findOne({_id: req.body.oferta_seleccionada}, function (err, oferta) {
            if (err) throw err;
            res.render('detalles_oferta', {oferta: oferta});

        });

    });

    /** ABOGACIA **/

    // GET de abogacia
    router.get('/abogacia', isAuthenticated, function (req, res) {
        // Redirect a 404 si el usuario no es un abogado
        if (req.session.user.Tipo != 'abogado')
            return res.redirect('/404');

        // Mostrar pagina de abogacia
        res.render('abogacia');
    });

    // GET de responder consultas
    router.get('/responder_consultas', isAuthenticated, function (req, res) {
        // Redirect a 404 si el usuario no es un abogado
        if (req.session.user.Tipo != 'abogado')
            return res.redirect('/404');

        // Buscar todas las consultas sin respuestas y mostrar al abogado
        Consulta.find({Respuesta: {$exists: false}}, function (err, consultas) {
            if (err) {
                console.log('ERROR QUERY DE CONSULTAS PENDIENTES\n' + err);
                // throw err;
                return res.render('abogacia', {
                    error: 'Se ha producido un error en nuestros servidores. Por favor, intentalo más tarde'
                });
            }

            // Si la petición viene despúes de responder a una consulta, notificamos que la consulta se ha respondido bien
            if (req.query.reply_success) {
                return res.render('responder_consultas', {consultas: consultas, message: 'Consulta respondida'});

                // Si la petición viene despúes de tener algun error en el servidor, avisar al abogado que se ha producido algun error
            } else if (req.query.server_error) {
                return res.render('responder_consultas', {
                    consultas: consultas,
                    error: 'Se ha producido un error en nuestros servidores. Por favor, intentalo más tarde'
                });
            }
            res.render('responder_consultas', {consultas: consultas});
        });

    });

    // Endpoint para responder consultas
    router.post('/responder_consultas', function (req, res) {

        // Buscar la consulta a responder a tràves de su id
        Consulta.findById(req.body.consulta_id, function (err, consulta) {
            if (err) {
                console.log('ERROR BUSCANDO CONSULTA\n' + err);
                // throw  err;
                // Volver a get de responder_consultas con aviso de que se ha producido algun error en el servidor
                return res.redirect('/responder_consultas?server_error=true');
            }

            // Guardar la respuesta
            consulta.Respuesta = req.body.respuesta;

            // Guardar los datos del abogado que ha contestado la consulta
            consulta.Abogado = {
                _id: req.session.user._id,
                Nombre: req.session.user.Nombre,
                Apellidos: req.session.user.Apellidos,
                Email: req.session.user.Email
            };

            // Actualizar la consulta
            consulta.save(function (err) {
                if (err) {
                    console.log('ERROR RESPONDIENDO CONSULTA\n' + err);
                    // throw  err;
                    // Volver a get de responder_consultas con aviso de que se ha producido algun error en el servidor
                    return res.redirect('/responder_consultas?server_error=true');
                }

                console.log('Consulta respondida ' + consulta);

                // Mensaje para enviar
                var htmlMessage = '<h3>Tu consulta en E-Migration ha sido respondida</h3>' +
                        '<p><strong>Respondida por: </strong>' + consulta.Abogado.Nombre + ' ' + consulta.Abogado.Apellidos + ' ' + '&lt;' + consulta.Abogado.Email + '&gt;' + '</p>' +
                        '<p><strong>Respuesta: </strong>' + consulta.Respuesta + '</p>' +
                        '<hr>' +
                        '<h3>Tu consulta</h3>' +
                        '<p><strong>Asunto: </strong>' + consulta.Asunto + '</p>' +
                        '<p><strong>Consulta completa: </strong>' + consulta.Consulta + '</p>',
                    message = {
                        from: 'E-Migration <support@emigration.com>',
                        to: consulta.Usuario.Email,
                        subject: 'Tu consulta en E-Migration ha sido respondida',
                        html: htmlMessage
                    };

                // Enviar el mensaje
                transporter.sendMail(message, function (err) {
                    if (err) {
                        console.log('ERROR SENDING MAIL\n' + err);
                        // throw err;
                        // Volver a get de responder_consultas con aviso de que se ha producido algun error en el servidor
                        return res.redirect('/responder_consultas?server_error=true');
                    }

                    console.log('Mail sent');
                });
                // Redirect a responder_consultas con variable replysuccess para notificar al abogado sobre la consulta que ha respondido él
                res.redirect('/responder_consultas?reply_success=true');
            });
        });
    });

    // GET de revisar consultas
    router.get('/revisar_consultas', isAuthenticated, function (req, res) {
        // Redirect a 404 si el usuario no es un abogado
        if (req.session.user.Tipo != 'abogado')
            return res.redirect('/404');

        // Buscar las consultas que hay que revisar
        Consulta.find({Falta_para_revisar: {$exists: true}}, function (err, consultas) {
            if (err) {
                console.log('ERROR QUERY DE CONSULTAS ' + err);
                // throw err;
                return res.render('abogacia', {
                    error: 'Se ha producido un error en nuestros servidores. Por favor, intentalo más tarde'
                });
            }

            // Si la petición viene despúes de revisar una consulta, notificamos que la consulta se ha revisado bien
            if (req.query.review_success) {
                return res.render('revisar_consultas', {
                    consultas: consultas,
                    message: 'Consulta revisada correctamente'
                });

                // Si la petición viene despúes de tener algun error en el servidor, avisar al abogado que se ha producido algun error
            } else if (req.query.server_error) {
                return res.render('revisar_consultas', {
                    consultas: consultas,
                    error: 'Se ha producido un error en nuestros servidores. Por favor, intentalo más tarde'
                });
            }
            res.render('revisar_consultas', {consultas: consultas});
        });
    });

    // Endpoint para guardar las consultas revisadas
    router.post('/revisar_consultas', function (req, res) {
        // Buscar la consulta que un abogado ha revisado por id
        Consulta.findById(req.body.consulta_id, function (err, consulta) {
            if (err) {
                console.log('ERROR BUSCANDO CONSULTA PARA REVISAR\n' + err);
                return res.redirect('/revisar_consultas?server_error=true');
            }

            // Guardar la nueva respuesta
            consulta.Respuesta = req.body.respuesta;

            // Guardar los datos del abogado que ha revisado la consulta
            consulta.Abogado = {
                _id: req.session.user._id,
                Nombre: req.session.user.Nombre,
                Apellidos: req.session.user.Apellidos,
                Email: req.session.user.Email
            };

            // Eliminar el flag sobre la consulta
            consulta.Falta_para_revisar = undefined;

            // Guardar la fecha de revisión de la consulta
            consulta.Fecha_revisada = new Date();

            // Actualizar la consulta, guardando los nuevos campos
            consulta.save(function (err) {
                if (err) {
                    console.log('ERROR ACTUALIZANDO LA CONSULTA\n ' + err);
                    // throw err;
                    return res.redirect('/revisar_consultas?server_error=true');
                }

                console.log('Consulta revisada correctamente\n ' + consulta);

                // Crear una cadena de la fecha de revisión con formato
                var fechaFormateada = consulta.Fecha_revisada.getDate() +
                    '/' + consulta.Fecha_revisada.getMonth() +
                    '/' + consulta.Fecha_revisada.getFullYear() +
                    ' - ' + consulta.Fecha_revisada.getHours() +
                    ':' + consulta.Fecha_revisada.getMinutes();

                // Mensaje para enviar
                var htmlMessage = '<h3>La respuesta a tu consulta en E-Migration ha sido revisada</h3>' +
                        '<p><strong>Revisada por: </strong>' + consulta.Abogado.Nombre + ' ' + consulta.Abogado.Apellidos + ' ' + '&lt;' + consulta.Abogado.Email + '&gt;' + '</p>' +
                        '<p><strong>Respuesta: </strong>' + consulta.Respuesta + '</p>' +
                        '<p style="text-align: right"><strong>(Revisado: ' + fechaFormateada + ')</strong></p>' +
                        '<hr>' +
                        '<h3>Tu consulta</h3>' +
                        '<p><strong>Asunto: </strong>' + consulta.Asunto + '</p>' +
                        '<p><strong>Consulta completa: </strong>' + consulta.Consulta + '</p>',
                    message = {
                        from: 'E-Migration <support@emigration.com>',
                        to: consulta.Usuario.Email,
                        subject: 'La respuesta a tu consulta en E-Migration ha sido revisada',
                        html: htmlMessage
                    };

                // Enviar el mensaje
                transporter.sendMail(message, function (err) {
                    if (err) {
                        console.log('ERROR SENDING MAIL ' + err);
                        // throw err;
                        return res.redirect('/revisar_consultas?server_error=true');
                    }

                    console.log('Mail sent');
                });

                // Redirect a revisar consultas con variable de review_success para informar al abogado sobre la consulta que acaba de revisar
                res.redirect('/revisar_consultas?review_success=true');
            });
        });
    });

    // GET de rehabilitar usuarios
    router.get('/rehabilitar_usuarios', isAuthenticated, function (req, res) {
        // Redirect a 404 si el usuario no es un abogado
        if (req.session.user.Tipo != 'abogado')
            return res.redirect('/404');

        // Buscar los usuarios con favorable 0
        Usuarios.find({Favorable: 0}, function (err, usuarios) {
            if (err) {
                console.log('ERROR BUSCANDO USUARIO\n' + err);
                // throw err;
                return res.render('abogacia', {error: 'Se ha producido un error en nuestros servidores. Por favor, intentalo más tarde'});
            }
            console.log(usuarios);

            // return null si no ha encontrado ningun usuario
            if (!usuarios.length) {
                return res.render('rehabilitar_usuarios', {usuarios: null});
            }

            // Si la petición viene despúes de tener algun error en el servidor, avisar al abogado que se ha producido algun error
            if (req.query.server_error) {
                return res.render('rehabilitar_usuarios', {
                    usuarios: usuarios,
                    error: 'Se ha producido un error en nuestros servidores. Por favor, intentalo más tarde'
                });
                // Si la petición viene despúes de rehabilitar a un usuario, notificamos que se ha ejecutado bien
            } else if (req.query.rehabilitar_success) {
                return res.render('rehabilitar_usuarios', {
                    usuarios: usuarios,
                    message: 'Usuario rehabilitado correctamente'
                });
            }
            return res.render('rehabilitar_usuarios', {usuarios: usuarios});
        });
    });

    // Endpoint para rehabilitar usuarios
    router.post('/rehabilitar_usuarios', function (req, res) {

        // Buscar el usuario para habilitar
        Usuarios.findByIdAndUpdate(
            req.body.usuario_id,
            {$set: {Favorable: 1}},
            function (err, usuario) {
                if (err) {
                    console.log('ERROR ACTUALIZANDO USUARIO\n' + err);
                    // throw err;
                    return res.redirect('/rehabilitar_usuarios?server_error=true');
                }
                console.log('Usuario actualizado\n' + usuario);

                // redirect al get de rehabilitar_usuarios con variable de rehabilitar_success
                return res.redirect('/rehabilitar_usuarios?rehabilitar_success=true')
            });
    });


    /** ABOGACIA END **/

    /** CONSULTAS **/
    router.get('/consultas', function (req, res) {
        // Buscar todas las consultas sin respuesta
        Consulta.find(
            {Respuesta: {$exists: true}, Es_privado: {$exists: false}, Falta_para_revisar: {$exists: false}},
            function (err, consultas) {
                if (err) {
                    console.log('ERROR QUERY DE CONSULTAS\n' + err);
                    // throw err;
                    return res.render('abogacia', {
                        error: 'Se ha producido un error en nuestros servidores. Por favor, intentalo más tarde'
                    });
                }

                // Si la petición viene despúes de enviar una consulta, notificamos que la consulta se ha enviado bien
                if (req.query.send_success) {
                    res.render('consultas', {consultas: consultas, message: 'Tu consulta ha sido enviada'});
                } else if (req.query.flag_limit) {
                    res.render('consultas', {
                        consultas: consultas,
                        error: 'Tu ultimo flag fue hecho hace menos de un día'
                    });
                } else if (req.query.flag_success) {
                    res.render('consultas', {consultas: consultas, message: 'Consulta enviada para revisar'});
                    // Si la petición viene despúes de tener algun error en el servidor, avisar al abogado que se ha producido algun error
                } else if (req.query.server_error) {
                    res.render('consultas', {
                        consultas: consultas,
                        error: 'Se ha producido un error en nuestros servidores. Por favor, intentalo más tarde'
                    });
                } else {
                    res.render('consultas', {consultas: consultas});
                }

            }
        );
    });

    // Endpoint para enviar consultas
    router.post('/consultas', function (req, res) {

        // Crear nueva consulta
        var newConsulta = new Consulta(
            {
                Asunto: req.body.asunto,
                Consulta: req.body.consulta_mensaje
            }
        );

        // Si la consulta es privada, guardar los campos necesarios
        if (req.body.tipo_consulta == 'privado') {
            newConsulta.Es_privado = true;
            newConsulta.Usuario._id = req.session.user._id;
            newConsulta.Usuario.Email = req.session.user.Email
        } else { // Si es publica, solo guardar el email
            newConsulta.Usuario.Email = req.body.email.trim();
        }

        // Guardamos la nueva consulta en la bbdd
        newConsulta.save(function (err) {
            if (err) {
                console.log('ERROR GUARDANDO CONSULTA\n' + err);
                // throw err;
                return res.redirect('/consultas?server_error=true');
            }
            // Mensaje para enviar
            var htmlMessage = '<h3>Tu consulta en E-Migration ha sido enviada. Recibirás otro mensaje cuando un abogado te responda la consulta.</h3>' +
                    '<p><strong>Asunto: </strong>' + newConsulta.Asunto + '</p>' +
                    '<p><strong>Consulta completa: </strong>' + newConsulta.Consulta + '</p>',
                message = {
                    from: 'E-Migration <support@emigration.com>',
                    to: newConsulta.Usuario.Email,
                    subject: 'Tu consulta en E-Migration ha sido enviada',
                    html: htmlMessage
                };

            // Enviar el mensaje
            transporter.sendMail(message, function (err) {
                if (err) {
                    console.log('ERROR SENDING MAIL ' + err);
                    // throw err;
                    return res.redirect('/consultas?server_error=true');
                }

                console.log('Mail sent');
            });

            // Redirect a consultas con variable sendsuccess para notificar al usuario sobre su consulta
            return res.redirect('/consultas?send_success=true');
        });
    });

    // Endpoint para poner flags a las consultas
    router.post('/revisar_consulta', function (req, res) {

        // Buscar el usuario que ha puesto el flag
        Usuarios.findById(req.session.user._id, function (err, user) {
            if (err) {
                console.log('ERROR BUSCANDO USUARIO\n' + err);
                // throw err;
                return res.redirect('/consultas?server_error=true');
            }

            // Mirar si tiene el campo Ultimo_flag
            if (user.Ultimo_flag) {

                // No le dejamos poner flag si el ultimo flag fue hecho hace menos de un día
                if (new Date() - user.Ultimo_flag < 86400000) {
                    return res.redirect('/consultas?flag_limit=true');
                }
            }

            // Actualizar el campo ultimo flag
            user.Ultimo_flag = new Date();

            // Actualizar el campo ultimo flag del usuario en la bbdd
            user.save(function (err) {
                if (err) {
                    console.log('ERROR ACTUALIZANDO USUARIO\n' + err);
                    // throw err;
                    return res.redirect('/consultas?server_error=true');
                }
                console.log('usuario actualizado:  ' + user);
            });

            // Buscar la consulta a marcar para revisar
            Consulta.findById(req.body.consulta_id, function (err, consulta) {
                if (err) {
                    console.log('ERROR BUSCANDO CONSULTA\n' + err);
                    // throw err;
                    return res.redirect('/consultas?server_error=true');
                }

                // Crear campo Falta_para_revisar
                consulta.Falta_para_revisar = true;

                // Actualizar la consulta, guardando el campo nuevo
                consulta.save(function (err) {
                    if (err) {
                        console.log('ERROR PONIENDO FLAG A CONSULTA ' + err);
                        // throw err;
                        return res.redirect('/consultas?server_error=true');
                    }
                    console.log('Consulta reportada');
                    res.redirect('/consultas?flag_success=true')
                });
            });
        });
    });
    /** CONSULTAS END **/

    // RUTAS
    router.get('/empadronarse', function (req, res) {
        res.render('empadronarse');
    });

    router.get('/estudiar', function (req, res) {
        res.render('estudiar_idiomas');
    });

    router.get('/tarjeta_sanitaria', function (req, res) {
        res.render('tarjeta_sanitaria');
    });

    router.get('/fuentes', function (req, res) {
        res.render('fuentes');
    });

    router.get('/privacy_policy', function (req, res) {
        res.render('privacy_policy');
    });

    router.get('/terms_conditions', function (req, res) {
        res.render('terms_conditions');
    });

//############## apuntarse en ofertas ###############################33
    router.post('/oferta_seleccionada', function (req, res) {
        Usuarios.findById(req.session.user._id, function (err, usuario) {
            if (err)
                return res.redirect('/empleo?server_error=true');
            //si el usuario es apto solo tendra que subir su curriculim
            console.log(usuario.Favorable);
            if (usuario.Favorable && usuario.Favorable == 1) {
                return res.render('file_upload_favorable', {idOferta: req.body.oferta});
            } else if (usuario.Favorable == 0) {

                //el usuario no cumple los requisitos
                return res.render('no_favorables', {
                    'message': 'No cumples los requisitos exigidos'
                });
            }
            if (!usuario.Dni_nie) {
                //aun no le ha procesado su situacion
                return res.render('file-upload', {idOferta: req.body.oferta});
            } else {
                res.render('no_favorables',
                    {
                        'message': 'Si ya tienes documento de identificación, no puedes apuntarte en nuestas ofertas!'
                    }
                );
            }
        });


    });

    router.post('/file-upload', function (req, res) {
            var sampleFile;
            if (!req.files) {
                res.send('No se ha subido ningun fichero.');
                return;
            }

            sampleFile = req.files;
            console.log(sampleFile);
            var usuario = req.session.user._id;
            var personalDirectory = usuario + '/' + req.body.id + '/'; //carpeta personal(unico)
            var dir = 'public/documentos/' + personalDirectory;         //ruta carpeta personal
            var ext = '.' + sampleFile.file.name.split('.')[1];
            //cambiamos el nombre de fichero
            var filename = Object.keys(req.body)[1] + ext;
            //comprobar si el directorio existe, si es asi lo borramos antes de subir los nuevos ficheros

            if (fs.existsSync(dir)) {
                fs.readdir(dir, function (err, items) {
                    console.log(items);

                    for (var i = 0; i < items.length; i++) {
                        console.log(items[i]);
                        if (items[i].split('.')[0] == filename.split('.')[0]) {
                            rimraf(dir + items[i], function () {
                                console.log('done');
                                i = items.length;
                            });
                        }
                    }

                });
            }


            mkdirp('public/documentos/' + personalDirectory, function (err) {

                if (err) return console.error(err);

                sampleFile.file.mv(dir + filename, function (err) {
                    if (err) {
                        console.log(err);
                        return res.render('file-upload', {err: 'Un error ha occurrido mientras se subia el fichero!'});
                    }
                    return res.render('file-upload', {message: 'Documento subido correctamente !'});

                });
            });

//mirar valor favorable
//añadimos la oferta en la lista de las ofertas inscritas del usuario
            Usuarios.findOneAndUpdate(
                {_id: usuario},
                {
                    $addToSet: {
                        Ofertas_inscritas: req.body.id
                    }
                },
                function (err) {
                    if (err)
                        console.log(err);
                }
            );

//añadir los datos del usuario en el array Inscritos de la oferta
            if(req.session.user.Favorable) {
                Ofertas.findOneAndUpdate(
                    {
                        _id: req.body.id
                    },
                    {
                        $addToSet: {
                            Inscritos: {
                                _id: req.session.user._id,
                                Nombre_usuario: req.session.user.Nombre,
                                Apellidos_usuario: req.session.user.Apellidos,
                                Email_usuario: req.session.user.Email,
                                Favorable: 1
                            }
                        }
                    },
                    function (err) {
                        if (err)
                            console.log(err);
                    }
                );
            } else {
                Ofertas.findOneAndUpdate(
                    {
                        _id: req.body.id
                    },
                    {
                        $addToSet: {
                            Inscritos: {
                                _id: req.session.user._id,
                                Nombre_usuario: req.session.user.Nombre,
                                Apellidos_usuario: req.session.user.Apellidos,
                                Email_usuario: req.session.user.Email
                            }
                        }
                    },
                    function (err) {
                        if (err)
                            console.log(err);
                    }
                );
            }

        }
    );
//############################### FIN APUNTARSE A OFERTAS ####################################333n


//# lISTAR OFERTAS QUE TIENEN CANDIDATOS PARA MOSTRAR SUS PARTICIPANTES A LOS ABOGADOS
    router.get('/ofertas_abiertas', isAuthenticated, function (req, res) {
        var usuario = req.session.user;

        if (usuario.Tipo == 'abogado') {
            //mostramos para cada oferta el nombre del participante junto con el padro y el certificado
            Ofertas.find(
                {
                    Plazas: {$gt: 0},
                    Inscritos: {$ne: []}
                },
                function (err, ofertas) {
                    if (err)
                        console.log(err);
                    if (req.query.server_error) {
                        return res.render(
                            'ofertas_abiertas',
                            {
                                ofertas: ofertas,
                                title: 'Ofertas en proceso',
                                error: 'Se ha producido un error en nuestros servidores, vuelve a intentarlo más tarde'
                            }
                        );
                    }
                    if (req.query.validated) {
                        return res.render(
                            'ofertas_abiertas',
                            {
                                ofertas: ofertas,
                                title: 'Ofertas en proceso',
                                ok: 'Usuario validado'
                            }
                        );
                    }

                    return res.render(
                        'ofertas_abiertas',
                        {
                            ofertas: ofertas,
                            title: 'Ofertas en proceso',
                        }
                    );

                });

        }
        else {
            res.redirect('/404');
        }
    });


//recuperar la validacion de los abogados y actualizar los datos del usuario
    router.post('/ofertas_abiertas', function (req, res) {
        //buscar el usuario y ponerle la nota de los abogados
        Usuarios.findOneAndUpdate(
            {
                _id: req.body.idusuario
            },
            {
                $set: {
                    Favorable: req.body.validar
                }
            },
            function (err) {
                if (err)
                    return res.redirect('/ofertas_abiertas?server_error=true');

            }
        );

        //ahora mirar en todas las ofertas donde se ha apuntado el usuario añadir el campo favorable
        Ofertas.find({Inscritos: {$elemMatch: {_id: req.body.idusuario}}}, function (err, ofertas) {
            if (err) {
                return res.redirect('/ofertas_abiertas?server_error=true');
            }
            ofertas.forEach(
                function (ofertaActual) {
                    if (req.body.validar == 1) {
                        for (var i = 0; i < ofertaActual.Inscritos.length; i++) {
                            if (ofertaActual.Inscritos[i]['_id'] == req.body.idusuario) {
                                ofertaActual.Inscritos[i]['Favorable'] = req.body.validar;
                            }
                        }
                    }
                    else {
                        var newInscritos = [];
                        for (var i = 0; i < ofertaActual.Inscritos.length; i++) {
                            if (ofertaActual.Inscritos[i]['_id'] != req.body.idusuario) {
                                newInscritos.push(ofertaActual.Inscritos[i]);
                            }
                        }
                        ofertaActual.Inscritos = newInscritos;

                    }

                    Ofertas.find({_id: ofertaActual._id}, function (err, oferta) {
                        if (err) {
                            return res.redirect('/ofertas_abiertas?server_error=true');
                        }
                        oferta = ofertaActual;
                        oferta.save(function (err) {
                            if (err)
                                return res.redirect('/ofertas_abiertas?server_error=true');
                        })
                    });

                }
            );
            return res.redirect('/ofertas_abiertas?validated=true')
        })
    });

    router.get('/download', isAuthenticated, function (req, res) {
        if (req.session.user.Tipo == 'empleador' || req.session.user.Tipo == 'abogado') {
            console.log(req.query.path);
            var file = req.query.path;
            var extensions = ['.pdf', '.doc', '.docx', '.odt', '.jpg', '.jpeg', '.png'];
            var file_exists = false;
            for (var i = 0; i < extensions.length && !file_exists; i++) {
                if (fs.existsSync(file + extensions[i])) {
                    console.log(file + extensions[i]);
                    file = file + extensions[i];
                    file_exists = true;
                }
            }
            if (file_exists) {
                console.log('origin: ', req.headers.referer);
                return res.download(file);
            }
            var url = req.headers.referer.split('?')[0];
            return res.redirect(url + '?message=Este fichero esta borrado o esta corrupto');
        } else {
            res.redirect('/404');
        }
    });

//MOSTRAR AL EMPLEADOR CONNECTADO SUS OFERTAS ACTIVAS PARA QUE PUEDA GESTIONARLAS
    router.get('/mis_ofertas', isAuthenticated, function (req, res) {

        if (req.session.user.Tipo == 'empleador') {
            //mostrar las ofertas del empleador con los candidatos validados(favorable: 1)
            Ofertas.find(
                {Id_empleador: req.session.user._id, Plazas: {$gt: 0}},
                function (err, misofertas) {
                    if (err)
                        console.log(err);
                    if (req.query.message) {
                        return res.render('mis_ofertas',
                            {
                                ofertas: misofertas,
                                title: "MIS OFERTAS",
                                message: req.query.message
                            });
                    }
                    return res.render('mis_ofertas', {ofertas: misofertas, title: "MIS OFERTAS"});

                });
        }
        else {
            return res.redirect('/404');
        }
    });

    router.post('/mis_ofertas', function (req, res) {

        console.log('id' + req.body.userId);
        var htmlContent = '<h2>Hola ' + req.body.userName + '</h2>';
        htmlContent += '<div class="text-justify">A continuación se mostra un email sobre la oferta <b>' + req.body.oferta + '</b></div><br>';
        htmlContent += '<div style="background-color: lightgrey;">Fecha de la entrevista <span class="small">' + req.body.fecha + '</span><br>' + req.body.contenido + '</div>';
        htmlContent += '<hr><p><b>Te deseamos mucha suerte!<br>Para confirmar asisténcia envia email al empresario al siguiente correo: ' + req.session.user.Email + '</b></p>';
        message = {
            from: 'E-Migration <support@emigration.com>',
            to: req.body.userMail,
            subject: req.body.subject,
            html: htmlContent
        };

        var datesplit = req.body.fecha.split('-'),
            strday = datesplit[0],
            strmonth = datesplit[1],
            stryear = datesplit[2].split(' ')[0],
            strtime = datesplit[2].split(' ')[1] + ':00',
            date = new Date(stryear + '-' + strmonth + '-' + strday + 'T' + strtime + 'Z');
        console.log(date);
        console.log(typeof date);
        console.log('date:', date);
        // Enviar el mensaje
        transporter.sendMail(message, function (err) {
            if (err) {
                console.log('ERROR SENDING MAIL ' + err);
                var url = req.headers.referer.split('?')[0];
                return res.redirect(url + '?message=Envio fallido, vuelve a intentarlo!');
            }

            Ofertas.findOneAndUpdate(
                {
                    _id: req.body.idOferta,
                    'Inscritos._id': req.body.userId
                },
                {
                    $set: {
                        "Inscritos.$.Entrevista.Fecha_Entrevista": date,
                        "Inscritos.$.Entrevista.Estado": 0
                    }
                },
                function (err, oferta) {
                    if (err) {
                        console.log(err);
                        var url = req.headers.referer.split('?')[0];
                        return res.redirect(url + '?message=Se ha producido un error en nuestros servidores, vuelve a intentarlo más tarde!');
                    }
                }
            );

            console.log('Mail sent');

            // Volver a login notificando al usuario sobre el mensaje reenviado
            var url = req.headers.referer.split('?')[0];
            return res.redirect(url + '?message=Mensaje enviado!');
        });


    });

//entrevistas
    router.post('/contratar', function (req, res) {
        var htmlcontent = '<h2>hola ' + req.body.username + '</h2>';
        htmlcontent += '<div class="text-justify">A continuación se mostra un email sobre la oferta <b>' + req.body.oferta + '</b></div><br>';

        if (req.body.contratar == 0) {
            htmlcontent += '<div style="background-color: red;"> Lo sentimos pero no has sido seleccionad@ </div>';
            htmlcontent += '<hr><p><b>te deseamos mucha suerte!<br>Sobretodo sigue apuntandote en las ofertas. </b></p>';
        } else {
            htmlcontent += '<div style="background-color: red;">Felicidades, has sido seleccionad@ para esta oferta </div>';
            htmlcontent += '<hr><p>Ponte en contacto  con el empresario a traves de este email: ' + req.session.user.Email + '</b></b></p>';
        }

        message = {
            from: 'E-Migration <support@emigration.com>',
            to: req.body.userMail,
            subject: 'Resultado oferta ' + req.body.oferta,
            html: htmlcontent
        };


        // Enviar el mensaje
        transporter.sendMail(message, function (err) {
            if (err) {
                console.log('ERROR SENDING MAIL ' + err);
                var url = req.headers.referer.split('?')[0];
                return res.redirect(url + '?message=Envio fallido, vuelve a intentarlo!!');
            }

            console.log('Mail sent');

        });
        console.log(req.body.userId);

        Ofertas.findById(
            req.body.idOferta,

            function (err, oferta) {
                if (err) {
                    console.log(err);
                    var url = req.headers.referer.split('?')[0];
                    return res.redirect(url + '?message=Se ha producido un error en nuestros servidores, vuelve a intentarlo más tarde!');
                }

                oferta.Usuarios_contratados.push(req.body.userId);
                oferta.Plazas--;

                oferta.save(function (err) {
                    if (err) {
                        console.log(err);
                        var url = req.headers.referer.split('?')[0];
                        return res.redirect(url + '?message=Se ha producido un error en nuestros servidores, vuelve a intentarlo más tarde!');
                    }

                    console.log('OFERTA ACTUALIZADA');
                    Usuarios.findOneAndUpdate({_id: req.body.userId},
                        {
                            $set: {Oferta_contratada: req.body.idOferta}
                        },
                        function (err, user) {
                            if (err) {
                                var url = req.headers.referer.split('?')[0];
                                return res.redirect(url + '?message=Se ha producido un error en nuestros servidores, vuelve a intentarlo más tarde!');
                            } else {
                                return res.redirect('/mis_ofertas');

                            }
                        }
                    );
                });


            });


    });

    /** RUTAS EN PROFILE **/
    router.get('/profile', isAuthenticated, function (req, res) {
        res.render('profile')
    });
    router.post('/change_password', function (req, res) {
        Usuarios.findById(req.body.id_user, function (err, user) {
            if (err) {
                console.log('ERROR BUSCANDO USUARIO\n' + err);
                // throw err;
                return res.render('profile', {error: 'Contraseña actual incorrecta'});
            }
            if (!bCrypt.compareSync(req.body.current_password, user.Contrasena)) {
                return res.render('profile', {error: 'Contraseña actual incorrecta'});
            }

            Usuarios.findOneAndUpdate(
                {_id: req.body.id_user},
                {$set: {Contrasena: bCrypt.hashSync(req.body.new_password, bCrypt.genSaltSync(10), null)}},
                function (err, doc) {
                    if (err) {
                        console.log('ERROR ACTUALIZANDO CONTRASEÑA\n' + err);
                        // throw err;
                        return res.render('profile', {error: 'Se ha producido un error en nuestros servidores. Por favor, intentalo más tarde'});
                    }
                    console.log('Usuario modificado:\n' + doc);
                    res.render('profile', {success: 'Contraseña cambiada'});
                }
            );
        })
    });

    router.post('/change_info', function (req, res) {
        Usuarios.findOneAndUpdate(
            {_id: req.body.id_user},
            {
                $set: {
                    Nombre: req.body.first_name,
                    Apellidos: req.body.last_name
                }
            },
            function (err, doc) {
                if (err) {
                    console.log('ERROR ACTUALIZANDO USUARIO\n' + err);
                    // throw err;
                    return res.render('profile', {error: 'Se ha producido un error en nuestros servidores. Por favor, intentalo más tarde'});
                }
                console.log('Usuario modificado: ' + doc);
                res.render('profile', {success: 'Información actualizada'});
            }
        );
    });

    router.post('/delete', function (req, res) {
        Usuarios.findOneAndRemove({_id: req.body.id_user}, function (err) {
            if (err) {
                console.log('ERROR ELIMINANDO USUARIO\n' + err);
                // throw err;
                return res.render('profile', {error: 'Se ha producido un error en nuestros servidores. Por favor, intentalo más tarde'});
            }

            res.redirect('/logout');
        });
    });
    /** FIN **/
    router.get('/regularizacion', function (req, res) {
        res.render('regularizacion');
    });

    router.get('/chat', function (req, res) {
        res.render('chat', {'website': req.get('host')});
    });

    router.get('*', function (req, res) {
        var notFoundUrl = req.protocol + '://' + req.get('host') + req.url;
        res.render('404', {url: notFoundUrl});
    });
    router.post('*', function (req, res) {
        var notFoundUrl = req.protocol + '://' + req.get('host') + req.url;
        res.render('404', {url: notFoundUrl});
    });


    return router;
}
;
