var express = require("express"),
    mongoose = require("mongoose"),
    bodyParser = require('body-parser');

var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var fileUpload = require('express-fileupload');


var app = express(),
    server = require('http').createServer(app),
    io = require('socket.io').listen(server),
    ent = require('ent'); // permite bloquear los caracteres HTML (sseguridad equivalente a htmlentities en PHP)

io.sockets.on('connection', function (socket, apodo) {
//al ingresar un apodo, le guardamos en variable de session y informamos a los demas conectados
    socket.on('nueva_connexion', function(apodo) {
        apodo = ent.encode(apodo);
        socket.apodo = apodo;
        socket.broadcast.emit('nueva_connexion', apodo);
    });

    // cuando llega un mensaje al servidor, recuperamos el apodo de su autor y lo transmitimos a los demas conectados
    socket.on('mensaje', function (mensaje) {
        mensaje = ent.encode(mensaje);
        socket.broadcast.emit('mensaje', {apodo: socket.apodo, mensaje: mensaje});
    });
});

app.use(fileUpload());
app.use(favicon(path.join(__dirname, '/public/img/logo.jpg')));
app.use(logger('dev'));
app.use(cookieParser());
app.use(express.static(path.join(__dirname + '/public')));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


//optional , el motor de renderitzar per defecte es jade
//app.engine('jade', require('jade').__express);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
mongoose.connect("mongodb://e-migration:emigration2017@ds111461.mlab.com:11461/e-migration", function(err){
    if(err){
        return console.log("Error de connexió amb la BBDD e-migration");
    }
    else{
        console.log("Connectat a e-migration");
    }
});

// Configuring Passport
var passport = require('passport');
var expressSession = require('express-session');
var MongoStore = require('connect-mongo')(expressSession);
// TODO - Why Do we need this key ?
app.use(expressSession({
    secret: 'mySecretKey',
    name: 'emigration',
    resave: false,
    saveUninitialized: false,
    store: new MongoStore({mongooseConnection: mongoose.connection})//,
    // cookie: {
    //     maxAge: 1000 * 7200
    // }
}));
app.use(passport.initialize());
app.use(passport.session());

// Using the flash middleware provided by connect-flash to store messages in session
// and displaying in templates
var flash = require('connect-flash');
app.use(flash());

app.use(function (req, res, next) {
    res.locals.user = req.user;
    next();
})

// Initialize Passport
var initPassport = require('./passport/init');
initPassport(passport);
var routes = require('./routes/controller')(passport);
app.use('/', routes);

//module.exports = app;
module.exports = server;
server.listen(3000, function(){
    console.log("Node server running on localhost:3000");
});



