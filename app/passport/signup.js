var LocalStrategy = require('passport-local').Strategy;
var User = require('../model/usuario');
var bCrypt = require('bcrypt-nodejs');
var nodemailer = require('nodemailer'),
    transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'emigration2wiaw@gmail.com',
            pass: 'emigration2017'
        }
    });

module.exports = function (passport) {

    passport.use('signup', new LocalStrategy({
            passReqToCallback: true // allows us to pass back the entire request to the callback
        },
        function (req, username, password, done) {
            var dataValidated = false;
            username = username.trim();
            var nombre = req.body.nombre.trim(),
                apellidos = req.body.apellidos.trim(),
                nif = req.body.nif.trim().toUpperCase(),
                dni_nie = req.body.dni_nie.trim().toUpperCase();

            // Comprobar si ha seguido el requisito en contrasena
            var pattern = /.{6,}/;
            if (!pattern.test(password)) {
                return done(null, false, req.flash('message', 'La contraseña tiene que tener como minimo 6 caracteres'));
            }

            if (req.body.tipos == 'empleador') {

                // Si es empleador, comprobar si el formato de nif está bien
                pattern = /^\w\d{8}$/;
                if (!pattern.test(nif)) {
                    return done(null, false, req.flash('message', 'Formato de NIF empresa incorrecto. Ejemplo: B12345677'));
                }
                dataValidated = true;

            } else if (req.body.dni_nie) {
                // Si es particular y tiene dni o nie, comprobar si está bien el formato
                pattern = /^[\w\d]\d{7}\w$/;
                if (!pattern.test(dni_nie)) {
                    return done(null, false, req.flash('message', 'Formato de DNI o NIE incorrecto. Ejemplo: 12345684A/X1234567W'));
                }
                dataValidated = true;

            } else {
                dataValidated = true;
            }

            if (dataValidated) {
                console.log('passed validation');

                findOrCreateUser = function () {
                    // find a user in Mongo with provided username
                    User.findOne({
                        $or: [
                            {Email: username},  // Mirar si el email puesto ya existe en la bbdd
                            {Nif_empresa: nif}, // Mirar si el nif puesto ya existe en la bbdd
                            {Dni_nie: dni_nie}  // Mirar si el dni/nie puesto ya existe en la bbdd
                        ]
                    }, function (err, user) {
                        // In case of any error, return using the done method
                        if (err) {
                            console.log('Error in SignUp:\n' + err);
                            // return done(err);
                            return res.render('register', {error: 'Se ha producido un error en nuestros servidores. Por favor, intentalo más tarde'});
                        }
                        // already exists
                        if (user) {
                            console.log('User already exists: ' + user);
                            return done(null, false, req.flash('message', 'Ya existe un usuario con esta información'));
                        }
                        // if there is no user with that email
                        // create the user
                        var newUser = new User(
                            {
                                Email: username,
                                Contrasena: createHash(password),
                                Nombre: nombre,
                                Apellidos: apellidos,
                                Tipo: req.body.tipos,
                                No_verificado: 1
                            }
                        );
                        if (req.body.tipos == 'empleador') {
                            newUser.Nif_empresa = nif;
                        } else {
                            newUser.Ofertas_inscritas = [];
                            if (req.body.dni_nie) {
                                newUser.Dni_nie = dni_nie;
                            }
                        }

                        console.log(newUser);

                        // save the user
                        newUser.save(function (err) {
                            if (err) {
                                console.log('Ha hagut mentre es guardaba l\'usuari en la BBDD:\n' + err);
                                // throw err;
                                return res.render('register', {error: 'Se ha producido un error en nuestros servidores. Por favor, intentalo más tarde'});
                            }
                            console.log('Usuario registrado correctamente');

                            var htmlMessage = '<h2>Bienvenido a E-Migration</h2>. <a href="http://localhost:3000/login?code=' + newUser._id
                            var website = req.protocol + '://' + req.get('host'),
                                htmlMessage = '<h2>Bienvenido a E-Migration</h2>' +
                                    '<a href="' + website + '/login?code=' + newUser._id
                                    + '">Clica aqui para verificar tu cuenta</a>',
                                message = {
                                    from: 'E-Migration <support@emigration.com>',
                                    to: username,
                                    subject: 'Verifica tu usuario en E-Migration',
                                    html: htmlMessage
                                };
                            transporter.sendMail(message, function (err, res) {
                                if (err) {
                                    console.log('ERROR SENDING MAIL ' + err);
                                    // throw err;
                                    return res.render('register', {error: 'Se ha producido un error en nuestros servidores. Por favor, intentalo más tarde'});
                                }

                                console.log('Mail sent');
                            });

                            return done(null, newUser, req.flash(
                                'message',
                                "Registro efectuado correctamente. Hay que verificar tu usuario a tràves del enlace enviado a tu correo electronico." +
                                "<br><a href='?email=" + username + "&send_code=" + newUser._id + "'>Reenviar mensaje</a>"));
                        });

                    });
                };

                // Delay the execution of findOrCreateUser and execute the method
                // in the next tick of the event loop
                process.nextTick(findOrCreateUser);
            }


        })
    );

    // Generates hash using bCrypt
    var createHash = function (password) {
        return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
    }

}