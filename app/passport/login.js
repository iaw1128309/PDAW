var LocalStrategy = require('passport-local').Strategy;
var User = require('../model/usuario');
var bCrypt = require('bcrypt-nodejs'); //para encryptar el password

module.exports = function (passport) {

    passport.use('login', new LocalStrategy({
            passReqToCallback: true
        },
        function (req, username, password, done) {
            username = username.trim();
            // check in mongo if a user with username exists or not
            User.findOne({'Email': username},
                function (err, user) {
                    // In case of any error, return using the done method
                    if (err) {
                        console.log('error in username query\n' + err);
                        // return done(err);
                        return res.render('loginpage', {error: 'Se ha producido un error en nuestros servidores. Por favor, intentalo más tarde'});
                    }

                    // User exists but wrong password, log the error
                    //console.log(user);
                    if (!user || !isValidPassword(user, password)) {
                        console.log('Contrasenya incorrecta');
                        return done(null, false, req.flash('error', 'Usuario o contraseña incorrecta !!!')); // redirect back to login page
                    }

                    if (user.No_verificado) {
                        console.log('Usuario no verificado');
                        return done(null, false, req.flash('error', 'Usuario no verificado' +
                            "<br><a href='?email=" + username + "&send_code=" + user._id + "'>Reenviar mensaje</a>")); // redirect back to login page
                    }
                    // User and password both match, return user from done method
                    // which will be treated like success
                    req.session.user = user;

                    // Set 3 months expiration if remember me is checked in login
                    if (req.body.remember) {
                        console.log('remember checked');
                        req.session.cookie.maxAge = 7884000000;
                    }
                    return done(null, user, req.flash('message', 'Bienvenido ' + user.Nombre + ' !!!'));
                }
            );

        })
    );


    var isValidPassword = function (user, password) {
        // return bCrypt.compareSync(password, user.password);
        return bCrypt.compareSync(password, user.Contrasena);
    }

}